package com.glearning.spring.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CabDriver {
	
	
	private String phoneNumber;
	
	
	private String name;
	
	public CabDriver(
			@Value("sudheer") String name, 
			@Value(value = "4444444444") String phoneNumber
			) {
		this.name = name;
		this.phoneNumber = phoneNumber;
	}

	public void trip(String from, String destination) {
		System.out.println("Commuting from "+ from +" to "+ destination);
		
	}

}
