package com.glearning.spring.beans;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component
//@Scope("prototype")
public class User {
	
	private String name;
	private int age;
	
	private List<String> phoneNumbers;
	
	//@PostConstruct
	public void setup() {
		System.out.println("This method should be called after the constructor has been called");
	}
	
	//public User(@Value("Harish")String name, @Value("22") int age) {
	public User(String name, int age) {
		this.name = name;
		this.age = age;
		System.out.println("Called the constructor");
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + "]";
	}
	
	
	
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	//@PreDestroy
	public void tearDown() {
		System.out.println("This method should be called before the object is garbage collected");
	}

}
