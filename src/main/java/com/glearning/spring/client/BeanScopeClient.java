package com.glearning.spring.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.glearning.spring.beans.User;

public class BeanScopeClient {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("application-context.xml");
		
		System.out.println("======Singleton==================");
		
		User userBean1 = applicationContext.getBean("user", User.class);
		
		User userBean2 = applicationContext.getBean("user", User.class);
		
		System.out.println("Are the two userBeans pointing to the same instance");
		
		
		System.out.println(userBean1 == userBean2);
		
		System.out.println("By default spring beans are singleton. all the bean references point to the same object");
		
		System.out.println("======Singleton==================");
		
		
		System.out.println("===========Prototype==================");
		
		User userBean3 = applicationContext.getBean("user", User.class);
		
		User userBean4 = applicationContext.getBean("user", User.class);
		
		System.out.println("Are the two userBeans pointing to the same instance");
		
		
		System.out.println(userBean3 == userBean4);

		System.out.println("===========Prototype==================");
		
		
		((AbstractApplicationContext)applicationContext).registerShutdownHook();
		
	}

}
