package com.glearning.spring.client;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.glearning.spring.beans.User;

public class Client {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("application-context.xml");
		
		User userBean = applicationContext.getBean("user", User.class);
		
		List<String> phoneNumbers = userBean.getPhoneNumbers();
		System.out.println(phoneNumbers);
		
		
		/*
		 * String[] beanNames = applicationContext.getBeanDefinitionNames(); for(String
		 * bean: beanNames) { System.out.println("Bean :: "+bean); }
		 */
		//System.out.println(userBean);
		
		((AbstractApplicationContext)applicationContext).registerShutdownHook();
		
	}

}
