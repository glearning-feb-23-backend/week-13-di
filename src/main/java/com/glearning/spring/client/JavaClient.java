package com.glearning.spring.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.glearning.spring.beans.AppConfig;
import com.glearning.spring.beans.Passenger;

public class JavaClient {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new AnnotationConfigApplicationContext(AppConfig.class);
		
		Passenger passenger 
			= applicationContext.getBean("passenger", Passenger.class);
		
		passenger.printDetails();
		passenger.commute("BTM-Layout", "KR-Puram");
		
		((AbstractApplicationContext)applicationContext).registerShutdownHook();
		
	}

}
