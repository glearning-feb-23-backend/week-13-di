package com.glearning.spring.client;

import com.glearning.spring.beans.CabDriver;
import com.glearning.spring.beans.Passenger;

public class PassengerClient {
	
	public static void main(String[] args) {
		// we are responsible for creating the instance 
		CabDriver kishore = new CabDriver("Kishore", "9874547878");
		CabDriver suraj = new CabDriver("Suraj", "884547878");
		
		// we are responsible for injecting cab driver instance to Passenger
		Passenger hari = new Passenger(kishore, "Praveen");
		hari.commute("Maratahalli", "Airport");
	}
}
